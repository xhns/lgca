var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var concat = require('gulp-concat');
var reload = browserSync.reload;
const image = require('gulp-image');
const imageResize = require('gulp-image-resize');

/**
 * Compile pug files into HTML
 */
gulp.task('templates', function() {
    var YOUR_LOCALS = {
        lgca: 'Laboratory of Gravitation Cosmology and Astrophysics of Ulyanovsk State Pedagogical University'
    };

    return gulp
        .src('./app/*.pug')
        .pipe(
            pug({
                locals: YOUR_LOCALS
            })
        )
        .pipe(gulp.dest('./dist/'));
});


gulp.task('image', function () {
  gulp.src('./app/img/*')
    .pipe(image())
    .pipe(gulp.dest('./dist/img/'));
});

//team images
gulp.task('team-image', function () {
  gulp.src('./app/img/team/*')
    .pipe(imageResize({
        width: 150,
        height: 250,
        crop: true,
        upscale: true,
        }))
    .pipe(image())
    .pipe(gulp.dest('./dist/img/team/'));
});

//activities images
gulp.task('act-image', function () {
  gulp.src('./app/img/activities/*')
    .pipe(imageResize({
        width: 650,
        height: 350,
        crop: true,
        upscale: true,
        }))
    .pipe(image())
    .pipe(gulp.dest('./dist/img/activities/thumbnails/'));
  gulp.src('./app/img/activities/*')
    .pipe(imageResize({
        width: 1920,
        height: 1080,
        crop: true,
        upscale: true,
        }))
    .pipe(image())
    .pipe(gulp.dest('./dist/img/activities/fullsize/'));
});

/**
 * Important!!
 * Separate task for the reaction to `.pug` files
 */
gulp.task('pug-watch', ['templates'], function() {
    return reload();
});

/**
 * Sass task for live injecting into all browsers
 */
gulp.task('sass', function() {
    return gulp
        .src([
			'./node_modules/bootstrap/scss/bootstrap.scss',
                        './node_modules/magnific-popup/src/css/main.scss',
			'./app/scss/*.scss'
				     ])
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(reload({ stream: true }));
});

// Move the javascript files into our /src/js folder
gulp.task('js', function() {
    return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/jquery-easing/dist/jquery.easing.1.3.umd.js',
    'node_modules/scrollreveal/dist/scrollreveal.js',
    'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
    './app/js/*.js'
    ])
    .pipe(concat('script.js'))
    .pipe(gulp.dest("./dist/js"))
    .pipe(reload({ stream: true }));
});

	/**
	 * Serve and watch the scss/pug files for changes
	 */
gulp.task('default', ['sass', 'js', 'templates', 'image', 'team-image', 'act-image'], function() {
	browserSync({ server: './dist' });

	gulp.watch('./app/scss/*.scss', ['sass']);
	gulp.watch('./app/*.pug', ['pug-watch']);
	gulp.watch('./app/js/*.js', ['js']);
	gulp.watch('./app/img/*', ['image']);
	gulp.watch('./app/img/team/*', ['team-image']);
	gulp.watch('./app/img/activities/*', ['act-image']);
});
